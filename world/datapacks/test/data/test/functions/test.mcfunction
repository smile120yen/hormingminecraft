execute at @e[tag=shoot] run particle minecraft:cloud ~ ~ ~ 0.1 0.1 0.1 0 5 force @a

#光っているエンティティの方向を向くようにしたい
execute as @e[tag=horming,tag=!BURN,tag=red] run execute at @s run tp @s ~ ~ ~ facing entity @e[nbt={ActiveEffects:[{Id:24b}]},limit=1,team=BLUE]

execute as @e[tag=horming,tag=!BURN,tag=blue] run execute at @s run tp @s ~ ~ ~ facing entity @e[nbt={ActiveEffects:[{Id:24b}]},limit=1,team=RED]

#8ブロック以内なら直線
#execute as @e[tag=shoot,tag=!BURN] run execute at @s if entity @e[nbt={ActiveEffects:[{Id:24b}]},distance=15..] run execute at @s run tp @s ^ ^0.2 ^
#execute as @e[tag=shoot,tag=!BURN] run execute at @s if entity @e[nbt={ActiveEffects:[{Id:24b}]},distance=10..] run execute at @s run tp @s ^ ^0.1 ^
#execute as @e[tag=shoot,tag=!BURN] run execute at @s if entity @e[nbt={ActiveEffects:[{Id:24b}]},distance=5..] run execute at @s run tp @s ^ ^0.1 ^

#8ブロック以内なら直線
execute as @e[tag=shoot,tag=!BURN] run execute at @s run execute at @s run tp @s ^ ^0.2 ^0.4
#execute as @e[tag=shoot,tag=!BURN] run execute at @s run execute at @s run tp @s ~ ~-0.1 ~

#ホーミング削除したい
#execute as @e[tag=horming,tag=!BURN,tag=red] run execute at @s if entity @e[nbt={ActiveEffects:[{Id:24b}]},distance=..2,team=BLUE] run tag @s remove horming
#execute as @e[tag=horming,tag=!BURN,tag=blue] run execute at @s if entity @e[nbt={ActiveEffects:[{Id:24b}]},distance=..2,team=RED] run tag @s remove horming

#光ってるやつ一体も居なければ消す
execute unless entity @e[nbt={ActiveEffects:[{Id:24b}]}] as @e[tag=shoot] run kill @s

#ブロックに埋まると爆発
execute as @e[tag=shoot,tag=!BURN] run execute at @s unless block ~ ~ ~ air run tag @s add BURN

#スニークしたときに召喚
execute at @a[scores={step_sneak=20..},team=RED] run summon armor_stand ~ ~1 ~ {Tags:["horming","shoot","red"],NoGravity:1b,Marker:1b,Invisible:1}
execute at @a[scores={step_sneak=20..},team=BLUE] run summon armor_stand ~ ~1 ~ {Tags:["horming","shoot","blue"],NoGravity:1b,Marker:1b,Invisible:1}

scoreboard players set @a[scores={step_sneak=20..}] step_sneak 0

execute at @e[nbt={ActiveEffects:[{Id:24b}]},team=BLUE] run tag @e[tag=horming,distance=..2,tag=red] add BURN
execute at @e[nbt={ActiveEffects:[{Id:24b}]},team=RED] run tag @e[tag=horming,distance=..2,tag=blue] add BURN

#creeperにエフェクト乗せると、滞留ポーションできちゃう
execute at @e[tag=BURN] run summon creeper ~ ~ ~ {NoGravity:1b,ExplosionRadius:2,Fuse:0,Invulnerable:1}
kill @e[tag=BURN]

execute as @e[type=minecraft:spectral_arrow] run data merge entity @s {damage:0}

team join RED @e[type=zombie]
team join BLUE @e[type=zombie]

team join RED @e[type=spider]
team join BLUE @e[type=spider]

team join RED @e[type=skeleton]
team join BLUE @e[type=skeleton]